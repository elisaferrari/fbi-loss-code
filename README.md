# README #

This repository contains the code for the FBI-loss paper.
Check the ipython notebook src/demo.ipynb for instructions.

## Dependencies ##

Aside from the typical packages such as tensorflow and keras, the code also requires the [Tensorflow Constrained Optimization](https://github.com/google-research/tensorflow_constrained_optimization) package.

## Code ##

The code used for testing the constrained optimization, the bias-resilient neural network and the invariant feature learning approaches has been taken and slightly modified from their respective repositories ([1](https://github.com/google-research/tensorflow_constrained_optimization), [2](https://github.com/QingyuZhao/BR-Net), [3](https://github.com/dcmoyer/invariance-tutorial)).

## Other datasets
The paper makes use of other publicly available datasets:

* ABIDE: [link](http://fcon_1000.projects.nitrc.org/indi/abide/)
* Credit card fraud detection: [link](https://www.kaggle.com/mlg-ulb/creditcardfraud?select=creditcard.csv)
* UCI ADULT dataset: [link](https://archive.ics.uci.edu/ml/datasets/adult)