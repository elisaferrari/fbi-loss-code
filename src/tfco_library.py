import tensorflow_constrained_optimization as tfco
import math
import numpy as np
from six.moves import xrange
import tensorflow as tf
import itertools


class ExampleProblem(tfco.ConstrainedMinimizationProblem):

    def __init__(self, labels, predictions, confounder, eo_upper_bound=0.15):
        self._labels = labels
        self._predictions = predictions
        self._confounder = confounder
        self._eo_upper_bound = eo_upper_bound
        # The number of positively-labeled examples.
        self._positive_count = tf.reduce_sum(self._labels)
        self._opposite_count = 1 + tf.reduce_sum(self._labels * (
            1-self._confounder)) + tf.reduce_sum((1-self._labels) * (self._confounder))

    @property
    def objective(self):
        return tf.keras.losses.binary_crossentropy(y_true=self._labels, y_pred=self._predictions, from_logits=False, label_smoothing=0)

    @property
    def constraints(self):
        return self.eo()

    def eo(self):
        eo_1 = tf.reduce_sum(((1-self._predictions) * (1-self._labels)
                              * self._confounder)) / tf.reduce_sum(self._confounder)

        eo_2 = tf.reduce_sum(((1-self._predictions) * (1-self._labels)
                              * (1-self._confounder))) / tf.reduce_sum((1-self._confounder))

        eo_3 = tf.reduce_sum(((self._predictions) * (self._labels)
                              * self._confounder)) / tf.reduce_sum(self._confounder)

        eo_4 = tf.reduce_sum(((self._predictions) * (self._labels) *
                              (1-self._confounder))) / tf.reduce_sum((1-self._confounder))

        eo = tf.abs(eo_1-eo_2) + tf.abs(eo_3-eo_4)
        return eo - self._eo_upper_bound

    @property
    def proxy_constraints(self):
        return self.eo()


def take_batch(n, iterat):
    return np.array([next(iterat) for i in range(n)])


def shuffle(arr):
    l = len(arr)
    a = arr[0::2]
    b = arr[1::2]
    c = np.empty(arr.shape, dtype=arr.dtype)
    c[0::2] = a[np.random.choice(range(len(a)), len(a), replace=False)]
    c[1::2] = b[np.random.choice(range(len(b)), len(b), replace=False)]
    return c


def shuffle_perm(l):
    arr = np.array(list(range(l)))
    c = shuffle(arr)
    return c


class TFCOContainer():
    def __init__(self, n_features, eo_upper_bound):
        self._graph = tf.Graph().as_default()
        self._session = tf.Session()
        self.model = tf.keras.Sequential([
            tf.keras.layers.Dense(24, activation='relu'),
            tf.keras.layers.Dense(8, activation='relu'),
            tf.keras.layers.Dense(2, activation='softmax')
        ])
        self._labels = tf.placeholder(
            tf.float32, shape=(None,), name="labels_tensor")
        self._confounder = tf.placeholder(
            tf.float32, shape=(None,), name="confounder_tensor")
        self._features = tf.placeholder(
            tf.float32, shape=(None, n_features), name="ft")
        self.predictions = self.model(self._features)[:, 0]

        self.problem = ExampleProblem(
            labels=self._labels,
            confounder=self._confounder,
            predictions=self.predictions,
            eo_upper_bound=eo_upper_bound,
        )

    def train(self, data, labels, confounder, optimizer, epochs):

        train_op = optimizer.minimize(self.problem)

        self._session.run(tf.global_variables_initializer())

        n_batch = 200
        for ep in range(epochs):
            print("starting epoch", ep)
            perm_outer = shuffle_perm(len(labels))
            males_infinite = itertools.cycle(confounder[perm_outer])
            labels_infinite = itertools.cycle(labels[perm_outer])
            dataset_infinite = itertools.cycle(data[perm_outer])
            for b in range(data.shape[0]//n_batch):
                perm = np.random.choice(range(n_batch), n_batch, replace=False)
                lab_t = take_batch(n_batch, labels_infinite).astype(
                    np.float32)[perm]
                lab_m = take_batch(n_batch, males_infinite).astype(
                    np.float32)[perm]
                d = {
                    self._labels: lab_t,
                    self._confounder: lab_m,
                    self._features: (take_batch(n_batch, dataset_infinite)[perm]).astype(np.float32),
                }
                #print("opposite count",session.run(problem._opposite_count,feed_dict=d))
                self._session.run(train_op, feed_dict=d)

    def predict(self, data):
        prediction_valid = self._session.run(self.model(self._features), feed_dict={
            self._features: data.astype(np.float32)
        })
        return prediction_valid


def build_optimizer(lr_1, lr_2):
    return tfco.ProxyLagrangianOptimizer(
        optimizer=tf.train.AdamOptimizer(learning_rate=lr_1),
        constraint_optimizer=tf.train.AdamOptimizer(learning_rate=lr_2)
    )
