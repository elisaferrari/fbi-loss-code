from keras.models import Sequential, Model
from keras.layers import Activation, Dense, Dropout, Flatten, UpSampling3D, Input, ZeroPadding3D, Lambda, Reshape
from keras.layers.normalization import BatchNormalization
from keras.layers import Conv3D, MaxPooling3D
from keras.losses import mse, binary_crossentropy
from keras.utils import plot_model
from keras.constraints import unit_norm, max_norm
from keras import regularizers
from keras import backend as K
from keras.optimizers import Adam

import tensorflow as tf

from sklearn.model_selection import StratifiedKFold
import numpy as np
import nibabel as nib
import scipy as sp
import scipy.ndimage
from sklearn.metrics import mean_squared_error, r2_score



import dcor

def inv_mse(y_true, y_pred):
    mse_value = K.sum(K.square(y_true-y_pred))

    return -mse_value

def inv_correlation_coefficient_loss(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = K.mean(x)
    my = K.mean(y)
    xm, ym = x-mx, y-my
    r_num = K.sum(tf.multiply(xm,ym))
    r_den = K.sqrt(tf.multiply(K.sum(K.square(xm)), K.sum(K.square(ym)))) + 1e-5
    r = r_num / r_den

    r = K.maximum(K.minimum(r, 1.0), -1.0)
    return 1 - K.square(r)

def correlation_coefficient_loss(y_true, y_pred):
    x = y_true
    y = y_pred
    mx = K.mean(x)
    my = K.mean(y)
    xm, ym = x-mx, y-my
    r_num = K.sum(tf.multiply(xm,ym))
    r_den = K.sqrt(tf.multiply(K.sum(K.square(xm)), K.sum(K.square(ym)))) + 1e-5
    r = r_num / r_den

    r = K.maximum(K.minimum(r, 1.0), -1.0)
    return K.square(r)

class GAN():
    
    def __init__(self,input_shape,lrs,n_compressed_features):
        self.lrs = lrs
        optimizer_regressor = Adam(self.lrs[0])
        optimizer_distiller = Adam(self.lrs[1])
        optimizer_classifier = Adam(self.lrs[2])

        L2_reg = 0.1
        self.n_compressed_features = n_compressed_features

        # Build and compile the cf predictorinv_inv
        self.regressor = self.build_regressor()
        self.regressor.compile(loss='binary_crossentropy', optimizer=optimizer_regressor)

        # Build the feature encoder
        input_data = Input(shape=input_shape, name='input_data')
        feature = Dense(64, activation='relu')(input_data)
        feature = Dense(16, activation='relu')(feature)
        feature_last = Dense(self.n_compressed_features, activation='relu')(feature)
        self.encoder = Model(input_data, feature_last)

        # For the distillation model we will only train the encoder
        self.regressor.trainable = False
        cf = self.regressor(feature_last)
        self.distiller = Model(input_data, cf)
        self.distiller.compile(loss=correlation_coefficient_loss, optimizer=optimizer_distiller)

        # Build and Compile the classifer  
        input_feature_clf = Input(shape=(self.n_compressed_features,), name='input_feature_dense')
        feature_clf = Dense(self.n_compressed_features//2, activation='tanh',kernel_regularizer=regularizers.l2(L2_reg))(input_feature_clf)
        feature_clf = Dense(self.n_compressed_features//4, activation='tanh',kernel_regularizer=regularizers.l2(L2_reg))(feature_clf)
        prediction_score = Dense(2, name='prediction_score',kernel_regularizer=regularizers.l2(L2_reg))(feature_clf)
        self.classifier = Model(input_feature_clf, prediction_score)

        # Build the entire workflow
        prediction_score_workflow = self.classifier(feature_last)
        label_workflow = Activation('softmax', name='r_mean')(prediction_score_workflow)
        self.workflow = Model(input_data, label_workflow)
        self.workflow.compile(loss='categorical_crossentropy', optimizer=optimizer_classifier,metrics=['accuracy'])

    def build_regressor(self):
        inputs_x = Input(shape=(self.n_compressed_features,))
        feature = Dense(self.n_compressed_features//2, activation='tanh')(inputs_x)
        feature = Dense(self.n_compressed_features//4, activation='tanh')(feature)
        cf = Dense(1,activation="tanh")(feature)

        return Model(inputs_x, cf)

    def train(self, training_data, labels, epochs=50, batch_size = 1000):
        confounder = labels[:,2]
        true_labels = labels[:,:2]

        for epoch in range(epochs):
            print("Epoch:",epoch)
            for b in range(100):
                idx_batch = np.random.choice(range(len(training_data)),batch_size,replace=False)
                training_batch = training_data.iloc[idx_batch]
                confounder_batch = confounder[idx_batch]
                true_labels_batch = true_labels[idx_batch]
                

                # ---------------------
                #  Train confounder predictor
                # ---------------------

                encoded_features = self.encoder.predict(training_data)
                r_loss = self.regressor.train_on_batch(encoded_features, confounder)

                # ---------------------
                #  Train Disstiller
                # ---------------------

                g_loss = self.distiller.train_on_batch(training_data, confounder)

                # ---------------------
                #  Train Encoder & Classifier
                # ---------------------

                c_loss = self.workflow.train_on_batch(training_data, true_labels)
            
    def predict(self, input_data):
        return self.workflow.predict(input_data)

