
import matplotlib.pyplot as plt
import os
import kl_tools
import tensorflow as tf
import keras.backend as K
import keras
import numpy as np

import pandas as pd
import numpy as np


# model validation
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import roc_auc_score

# imports for model building
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution3D, MaxPooling3D, ZeroPadding3D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam


def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.

    # Arguments
        args (tensor): mean and log of variance of Q(z|X)

    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


class MyCVAE():
    def __init__(self, input_shape, dim_z, dim_c, n_classes, beta, lbd, lr):
        self.beta = beta
        self.lbd = lbd
        self.learning_rate = lr
        self.dim_z = dim_z
        self.input_shape = input_shape
        self.dim_c = dim_c
        self.predictor = self.build_predictor(n_classes, summary=False)

        ACTIVATION = "tanh"

        input_x = keras.layers.Input(shape=[self.input_shape], name="x")

        enc_hidden_1 = keras.layers.Dense(
            64, activation=ACTIVATION, name="enc_h1")(input_x)
        enc_hidden_2 = keras.layers.Dense(
            16, activation=ACTIVATION, name="enc_h2")(enc_hidden_1)

        z_mean = keras.layers.Dense(
            self.dim_z, activation="tanh")(enc_hidden_2)
        z_log_sigma_sq = keras.layers.Dense(
            self.dim_z, activation="linear")(enc_hidden_2)

        z = keras.layers.Lambda(sampling, output_shape=(
            self.dim_z,), name='z')([z_mean, z_log_sigma_sq])

        # this is the concat operation!
        input_c = keras.layers.Input(shape=[self.dim_c], name="c")
        z_with_c = keras.layers.concatenate([z, input_c])
        z_mean_with_c = keras.layers.concatenate([z_mean, input_c])

        dec_h1 = keras.layers.Dense(16, activation=ACTIVATION, name="dec_h1")
        dec_h2 = keras.layers.Dense(64, activation=ACTIVATION, name="dec_h2")
        output_layer = keras.layers.Dense(self.input_shape, name="x_hat")

        dec_hidden_1 = dec_h1(z_with_c)
        dec_hidden_2 = dec_h2(dec_hidden_1)
        x_hat = output_layer(dec_hidden_2)

        cvae = keras.models.Model(
            inputs=[input_x, input_c], outputs=x_hat, name="ICVAE")

        print(cvae.summary())

##
# make a mean model for outputs
##

        mean_dec_hidden_1 = dec_h1(z_mean_with_c)
        mean_dec_hidden_2 = dec_h2(mean_dec_hidden_1)
        mean_x_hat = output_layer(mean_dec_hidden_2)

        mean_cvae = keras.models.Model(
            inputs=[input_x, input_c],
            outputs=mean_x_hat, name="mean_VAE",
        )

        self.mean_cvae = mean_cvae
        self.encoder = keras.models.Model(
            inputs=input_x,
            outputs=z, name="encoder",
        )

        ##
        # okay, now we have a network. Let's build the losses
        ##

        recon_loss = keras.losses.mse(input_x, x_hat)
        recon_loss *= self.input_shape  # optional, in the tutorial code though

        kl_loss = 1 + z_log_sigma_sq - K.square(z_mean) - K.exp(z_log_sigma_sq)
        kl_loss = K.sum(kl_loss, axis=-1)
        kl_loss *= -0.5

        kl_qzx_qz_loss = kl_tools.kl_conditional_and_marg(
            z_mean, z_log_sigma_sq, self.dim_z)

        # optional add beta param here
        # and cite Higgins et al.
        cvae_loss = K.mean((1 + self.lbd) * recon_loss +
                           self.beta*kl_loss + self.lbd*kl_qzx_qz_loss)

        cvae.add_loss(cvae_loss)

        opt = keras.optimizers.Adam(lr=self.learning_rate)

        cvae.compile(optimizer=opt, )

        self.model = cvae

    # training?
    def train_encoder(self, x, c,  epochs=50):
        # if not os.path.exists("mnist_icvae.h5"):
        self.model.fit(
            {"x": x, "c": c}, epochs=epochs
        )
        #    self.model.save_weights("mnist_icvae.h5")
        # else:
        #    self.model.load_weights("mnist_icvae.h5")

    def encode(self, x):
        return self.encoder.predict(x)

    def build_predictor(self, n_classes, summary):
        model = Sequential()
        model.add(Dense(24, activation='relu', input_shape=(self.dim_z,)))
        model.add(BatchNormalization())
        model.add(Dropout(0.2))
        model.add(Dense(8, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dropout(0.2))
        model.add(Dense(n_classes, activation='softmax'))
        model.compile(optimizer="adam",
                      loss=tf.keras.losses.CategoricalCrossentropy())
        if summary:
            print(model.summary())
        return model

    def fit(self, x, y, epochs):
        x_enc = self.encode(x)
        self.predictor.fit(x_enc, y, epochs=epochs)

    def predict(self, x):
        x_enc = self.encode(x)
        return self.predictor.predict(x_enc)


if __name__ == "__main__":
    params = {
        "beta": 0.1,
        "lambda": 1.0,
    }

    (train_x, train_y), (test_x, test_y) = mnist_dataset.get_data()

    dz = 16
    dc = mnist_dataset.NUM_LABELS
    i_s = mnist_dataset.IMG_DIM ** 2
    lr = 0.0005

    m = MyCVAE(input_shape=i_s, dim_z=dz, dim_c=dc,
               beta=params["beta"], lbd=params["lambda"], lr=lr)

    m.train_encoder(x=train_x, c=train_y)

    n_plot_samps = 10
    m.test_encoder(x=test_x[:n_plot_samps], c=test_y[:n_plot_samps])

    print(m.encode(x=test_x[:n_plot_samps], c=test_y[:n_plot_samps]))
