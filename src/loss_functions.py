import keras
import keras.backend as K
import tensorflow as tf


def build_corrected_ce(k):
    print('building corrected ce with k = {}'.format(k))
    if k == 1:
        def custom_loss(y_true, y_pred):
            return K.categorical_crossentropy(y_true[:, :2], y_pred[:, :2])
        return custom_loss
    else:
        def custom_loss(y_true, y_pred):
            kfloat = tf.cast(k, dtype=tf.float32)

            underrepr = tf.abs(y_true[:, 0]-y_true[:, 2])
            error_entity = tf.abs(y_pred[:, 0]-y_true[:, 0])

            esponent = underrepr*error_entity
            mult = tf.pow(kfloat, esponent)

            ce_2 = - tf.reduce_sum(y_true[:, :2] * tf.log(y_pred[:, :2]), 1)
            return tf.reduce_mean(mult*ce_2)
        return custom_loss


def build_fbi_loss(k, alpha):
    print('building FBI-loss with k = {} and alpha = {}'.format(k, alpha))
    if k == 1:
        def custom_loss(y_true, y_pred):
            return K.categorical_crossentropy(y_true[:, :2], y_pred[:, :2])
        return custom_loss
    else:
        def custom_loss(y_true, y_pred):
            kfloat = tf.cast(k, dtype=tf.float32)

            underrepr = tf.abs(y_true[:, 0]-y_true[:, 2])
            error_entity = tf.abs(y_pred[:, 0]-y_true[:, 0])

            esponent = underrepr*error_entity*alpha
            mult = tf.pow(kfloat, esponent)

            ce_2 = - tf.reduce_sum(y_true[:, :2] * tf.log(y_pred[:, :2]), 1)
            return tf.reduce_mean(mult*ce_2)
        return custom_loss


def build_corrected_focal_loss(k, alpha):
    print('building corrected focal loss with k = {} and alpha = {}'.format(k, alpha))
    if k == 1:
        def custom_loss(y_true, y_pred):
            return K.categorical_crossentropy(y_true[:, :2], y_pred[:, :2])
        return custom_loss
    else:
        def custom_loss(y_true, y_pred):
            kfloat = tf.cast(k, dtype=tf.float32)

            underrepr = tf.abs(y_true[:, 0]-y_true[:, 2])

            error_entity = tf.abs(y_pred[:, 0]-y_true[:, 0])
            focal = tf.pow(error_entity, alpha)

            mult = tf.pow(kfloat, underrepr)
            ce_2 = - tf.reduce_sum(y_true[:, :2] * tf.log(y_pred[:, :2]), 1)

            return tf.reduce_mean(ce_2*mult*focal)
        return custom_loss


def build_demographic_parity(k, eps):
    print('building demographic parity with k = {} and eps = {}'.format(k, eps))

    def custom_loss(y_true, y_pred):
        ce = K.categorical_crossentropy(y_true[:, :2], y_pred[:, :2])
        dp_1 = tf.reduce_sum(
            (y_pred[:, 0] * y_true[:, 2])) / tf.reduce_sum(y_true[:, 2])
        dp_2 = tf.reduce_sum(
            (y_pred[:, 0] * (1-y_true[:, 2]))) / tf.reduce_sum((1-y_true[:, 2]))
        dp = tf.abs(dp_1-dp_2)
        return ce + k * tf.maximum(dp-eps, 0)
    return custom_loss


def build_equalized_odds(k, eps):
    print('building equalized odds with k = {} and eps = {}'.format(k, eps))

    def custom_loss(y_true, y_pred):
        ce = K.categorical_crossentropy(y_true[:, :2], y_pred[:, :2])

        eo_1 = tf.reduce_sum(
            (y_pred[:, 0] * (1-y_true[:, 0]) * y_true[:, 2])) / tf.reduce_sum(y_true[:, 2])

        eo_2 = tf.reduce_sum(
            (y_pred[:, 0] * (1-y_true[:, 0]) * (1-y_true[:, 2]))) / tf.reduce_sum((1-y_true[:, 2]))

        eo_3 = tf.reduce_sum(
            ((1-y_pred[:, 0]) * (y_true[:, 0]) * y_true[:, 2])) / tf.reduce_sum(y_true[:, 2])

        eo_4 = tf.reduce_sum(
            ((1-y_pred[:, 0]) * (y_true[:, 0]) * (1-y_true[:, 2]))) / tf.reduce_sum((1-y_true[:, 2]))

        eo = tf.abs(eo_1-eo_2) + tf.abs(eo_3-eo_4)
        return ce + k * tf.maximum(eo-eps, 0)
    return custom_loss
