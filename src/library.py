import pandas as pd
import numpy as np


# model validation
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import roc_auc_score

# imports for model building
import keras
import keras.backend as K
import tensorflow as tf
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution3D, MaxPooling3D, ZeroPadding3D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam


def generate_data(n_batches, batch_size, nfeats, bias, noise=5, task_effect=1, confounder_effect=3):
    n_s = [int(batch_size*bias//2),
           int(batch_size*bias//2),
           int(batch_size*(1-bias)//2)
           ]

    data = np.random.rand(n_batches*batch_size, nfeats)*2*noise-noise

    metadata = np.repeat([
        np.array([0, 0]),
        np.array([1, 1]),
        np.array([0, 1]),
        np.array([1, 0])
    ], tuple(n_s + [batch_size-sum(n_s)]), axis=0)

    np.random.shuffle(metadata)

    metadata = np.tile(metadata, (n_batches, 1))

    train_data = np.hstack((data, metadata))

    # add task effect
    add_effect(data=train_data,
               label=train_data[:, -2],
               features_0=[10, 11, 12, 13],
               features_1=[3, 4, 5, 6],
               effect=task_effect
               )

    # add confounder effect
    add_effect(train_data, train_data[:, -1], [73, 74, 75, 76], [28, 29, 30, 31], confounder_effect)

    # prepare validations
    overg_val = np.vstack((
        np.hstack((np.random.rand(n_batches*batch_size//10, nfeats)*2*noise-noise,
                   np.tile([[0, 0]], (n_batches*batch_size//10, 1)))),
        np.hstack((np.random.rand(n_batches*batch_size//10, nfeats)*2*noise-noise,
                   np.tile([[1, 1]], (n_batches*batch_size//10, 1))))
    )
    )
    add_effect(overg_val, overg_val[:, -2], [10, 11, 12, 13], [3, 4, 5, 6], task_effect)
    add_effect(overg_val, overg_val[:, -1], [73, 74, 75, 76], [28, 29, 30, 31], confounder_effect)

    underg_val = np.vstack((
        np.hstack((np.random.rand(n_batches*batch_size//10, nfeats)*2*noise-noise,
                   np.tile([[0, 1]], (n_batches*batch_size//10, 1)))),
        np.hstack((np.random.rand(n_batches*batch_size//10, nfeats)*2*noise-noise,
                   np.tile([[1, 0]], (n_batches*batch_size//10, 1))))
    )
    )
    add_effect(underg_val, underg_val[:, -2], [10, 11, 12, 13], [3, 4, 5, 6], task_effect)
    add_effect(underg_val, underg_val[:, -1], [73, 74, 75, 76], [28, 29, 30, 31], confounder_effect)

    # build dataframes
    feature_names = ["feature_{:04d}".format(i) for i in range(
        train_data.shape[1]-2)] + ["class", "bias_feature"]

    dfs = {
        "train": pd.DataFrame(train_data, columns=feature_names),
        "val_overg": pd.DataFrame(overg_val, columns=feature_names),
        "val_underg": pd.DataFrame(underg_val, columns=feature_names)
    }
    return dfs


def add_effect(data, label, features_0, features_1, effect):
    for f in features_1:
        data[:, f] += effect * label
    for f in features_0:
        data[:, f] += effect * (1-label)


def get_base_model(input_shape, summary=False):
    model = Sequential()
    model.add(Dense(24, activation='relu', input_shape=(input_shape,)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Dense(8, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Dense(2, activation='softmax'))
    if summary:
        print(model.summary())
    return model


def get_merged_model(input_shape, summary=False):
    Sequential_model = get_base_model(input_shape, summary=False)
    image_input = Input(shape=(input_shape,))
    Sequential_Output = Sequential_model(image_input)
    input_for_loss = Input(shape=(1,))

    Merged_Output = keras.layers.concatenate(
        [Sequential_Output, input_for_loss], axis=1)

    model = Model([image_input, input_for_loss], Merged_Output)

    if summary:
        print(model.summary())
    return model


def get_features_and_labels(dataframe):
    real_features = dataframe[set(
        dataframe.columns) - set(["class", "bias_feature"])]
    features_with_bias = [real_features, dataframe['bias_feature']]
    labels = np.stack(
        (dataframe['class'], 1-dataframe['class'], dataframe['bias_feature']), axis=-1)
    return features_with_bias, labels


def get_predictions_fpr_tpr(model, dataframe):
    features, labels = get_features_and_labels(dataframe)
    predictions = model.predict(features)
    fpr, tpr, thr = roc_curve(labels[:, 0], predictions[:, 0])
    return predictions, fpr, tpr
